﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSI_project
{
    public class SymptomsTree
    {
        //public SymptomsNode root;

        public List<SymptomsNode> symList;
        public List<DiseaseNode> disList;

        public void Sort()
        {
            disList.Sort(
                (a, b) =>
                {
                    if (a.probability < b.probability)
                        return 1;
                    if (a.probability > b.probability)
                        return -1;
                    return 0;
                }
                );
        }

        public void CalculateResults(double value, int index)
        {
            if (symList[index].QuestionToAsk)
            {
                if (!symList[index].isLeaf)
                {
                    CalculateResults((value + symList[index].value) / 2, 2 * index + 1);
                    CalculateResults(((1 - value) + symList[index].value) / 2, 2 * index + 2);
                }
                else
                {
                    int constant = (symList.Count + 1) / 2 - 1;
                    int i = 2 * (index - constant);
                    disList.Add(new DiseaseNode(Symptoms.diseases[i], (value + symList[index].value) / 2));
                    disList.Add(new DiseaseNode(Symptoms.diseases[i], ((1 - value) + symList[index].value) / 2));
                }
            }
        }

        public SymptomsTree()
        {

            symList = new List<SymptomsNode> { new SymptomsNode(0, SymptomsType.Value, 0.8, 0.2), new SymptomsNode(1, SymptomsType.Value, 0.6, 0.2),
                               new SymptomsNode(2, SymptomsType.Value, 0.7, 0.3), new SymptomsNode(3, SymptomsType.Linear, 0.7, 0.3),
                               new SymptomsNode(4, SymptomsType.Value, 0.8, 0.2), new SymptomsNode(5, SymptomsType.Value, 0.8, 0.2),
                               new SymptomsNode(6, SymptomsType.Value, 0.6, 0.4), new SymptomsNode(7, SymptomsType.Value, 0.7, 0.3),
                               new SymptomsNode(2, SymptomsType.Value, 0.6, 0.2), new SymptomsNode(8, SymptomsType.Value, 0.6, 0.3),
                               new SymptomsNode(9, SymptomsType.Value, 0.6, 0.4), new SymptomsNode(10, SymptomsType.Value, 0.8, 0.2),
                               new SymptomsNode(11, SymptomsType.Value, 0.8, 0.2), new SymptomsNode(5, SymptomsType.Value, 0.7, 0.3),
                               new SymptomsNode(12, SymptomsType.Value, 0.7, 0.2), new SymptomsNode(13, SymptomsType.Value, 0.6, 0.2, true),
                               new SymptomsNode(11, SymptomsType.Value, 0.8, 0.1, true), new SymptomsNode(14, SymptomsType.Value, 0.6, 0.1, true),
                                new SymptomsNode(15, SymptomsType.Value, 0.5, 0.3, true), new SymptomsNode(16, SymptomsType.Value, 0.8, 0.7, true),
                                new SymptomsNode(17, SymptomsType.Value, 0.7, 0.6, true), new SymptomsNode(18, SymptomsType.YesNo, 1, 0, true),
                                new SymptomsNode(19, SymptomsType.Value, 0.8, 0.2, true), new SymptomsNode(20, SymptomsType.YesNo, 1, 0, true),
                                new SymptomsNode(21, SymptomsType.Value, 0.8, 0.1, true), new SymptomsNode(22, SymptomsType.YesNo, 1, 0, true),
                                new SymptomsNode(23, SymptomsType.Value, 0.7, 0.2, true), new SymptomsNode(20, SymptomsType.YesNo, 1, 0, true),
                                new SymptomsNode(24, SymptomsType.Value, 0.6, 0.4, true), new SymptomsNode(25, SymptomsType.Value, 0.8, 0.2, true),
                                new SymptomsNode(26, SymptomsType.Value, 0.6, 0.4, true)
                                };
            disList = new List<DiseaseNode>();
        }
    }
}
