﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSI_project
{
    /// <summary>
    /// A single symptom
    /// </summary>
    public class SymptomsNode
    {
        /// <summary>
        /// symptom name
        /// </summary>
        public string symptom;
        /// <summary>
        /// symptoms type
        /// </summary>
        public SymptomsType type;
        /// <summary>
        /// the value given by the user
        /// </summary>
        public double value;
        private bool _questionToAsk;
        private double _noValue;
        private double _yesValue;

        public bool QuestionToAsk
        {
            get { return _questionToAsk; }
            set { _questionToAsk = value; }
        }

        public double YesValue
        {
            get { return _yesValue; }
            set { _yesValue = value; }
        }
        public double NoValue
        {
            get { return _noValue; }
            set { _noValue = value; }
        }

        public bool isLeaf;
        public int index;

        public SymptomsNode(int index, SymptomsType type, double yesValue, double noValue, bool isLeaf=false)
        {
            this.symptom = Symptoms.questions[index];
            this.type = type;
            this.isLeaf = isLeaf;
            this.index = index;
            this.value = -1;
            YesValue = yesValue;
            NoValue = noValue;
            QuestionToAsk = true;
        }

    }
    public class DiseaseNode
    {
        public string disease;
        public double probability;

        public DiseaseNode(string d, double p)
        {
            disease = d;
            probability = p;
        }
    }

    /// <summary>
    /// A list of questions to be asked to the user
    /// It is sorted in the way that is in the binary tree
    /// If there is a comment it means the symtpom is repeated in the tree, but we don't ask userabout it again
    /// Tree looks like: 
    ///             0
    ///        1           2
    ///     3     4      5    6
    ///   7   8 9   10 11 ...
    /// </summary>
    public static class Symptoms
    {
        public static readonly string[] questions =  { /*0)*/"Bóle głowy", "Dreszcze/drgawki", "Duszności", "Gorączka",
            "Anhodemia", "Biegunka", "Senność", "Szum w uszach", /*8) duszności - 2*/ "Brak apetytu", "Nadpobudliwość", 
            "Ból brzucha", "Krwioplucie", /*13) biegunka - 5*/ "Uczucie suchych oczu", "Nagłe wystąpienie objawów", /*16) krwioplucie - 11*/
            "Wygięcie ciała w łuk", "Nadmierne spożywanie alkoholu",  "Myśli samobójcze", "Halucynacje", "Jest dzieckiem",
            "Widoczna utrata włosów", "Hiperglikemina", "Zgrubienia na szyi", "Bezgłos", "Ból w klatce piersiowej",
            /*27) hiperglikemia - 20*/ "Regularne przerywanie snu","Wydzielina z oka śluzowo-ropna", "Bóle mięsni i stawów"
            };
        public static readonly string[] diseases =
        {
            "Grypa", "Przeziębienie", "Zapalenie płuc", "Grypa żołądkowa", "Padaczka", "Anemia", "Alkoholizm",
            "Uraz głowy", "Depresja", "Anoreksja", "Schizofremia", "Nerwica", "ADHD", "CHAD", "TTM", "Napięciowe bóle głowy",
            "Przewlekłe zapalenie trzustki", "Niestrawność", "Rak tarczycy", "Lęk patologiczny", "Rak krtani", "Rak płuc",
            "Zatorowość płucna", "Astma oskrzelowa", "Cukrzyca", "Celiaka", "Bezsenność", "Przemęczenie", "Zaćma",
            "Zapalenie spojówek", "Osteoporoza", "Zdrowy"
        };
    }

    public enum SymptomsType
    {
        Value, //dla: reszty
        YesNo, //dla: dziecko, bezgłos, hiperglikemi
        Linear //dla: gorączka
    }
}
