﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace MSI_project
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        SymptomsTree tree;
        const string question = "W jakiej skali posiadasz: ";
        int counter = 0;
        public MainWindow()
        {

            InitializeComponent();
            this.DataContext = this;
            tree = new SymptomsTree();
            answerBox.Focus();
            nextQuestion();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var temp = getAnswer(answerBox.Text);
            if (temp == -1)
            {
                MessageBox.Show("Podaj poprawną wartość");
                return;
            }

            for (int i = counter; i < tree.symList.Count; i++)
            {
                if (tree.symList[i].index == tree.symList[counter].index)
                {
                    if (temp >= tree.symList[i].YesValue)
                        DetachQuestions(2 * i + 2);
                    if (temp <= tree.symList[i].NoValue)
                        DetachQuestions(2 * i + 1);
                    tree.symList[i].value = temp;
                }
            }
            counter++;
            nextQuestion();
            answerBox.Focus();
        }

        private void DetachQuestions(int i)
        {
            if (i < tree.symList.Count)
            {
                tree.symList[i].QuestionToAsk = false;

                if (2 * i + 1 < tree.symList.Count)
                {
                    DetachQuestions(2 * i + 1);
                    DetachQuestions(2 * i + 2);
                }
            }
        }

        private double getAnswer(string answer)
        {
            double value = -1;
            switch (tree.symList[counter].type)
            {
                case SymptomsType.Value:
                    int v;
                    if (!Int32.TryParse(answer, out v))
                        return -1;
                    //throw new ArgumentException("wrong input by user");
                    if (v < 0)
                        v = 0;
                    else if (v > 10)
                        v = 10; 
                    return (double)(v) / 10;
                case SymptomsType.YesNo:
                    if (answer.ToLower().Equals("tak")
                        || answer.ToLower().Equals("t"))
                    {
                        return 1;
                    }
                    else if (answer.ToLower().Equals("nie")
                        || answer.ToLower().Equals("n"))
                        return 0;
                    return -1;
                case SymptomsType.Linear:
                    NumberStyles style = NumberStyles.Number | NumberStyles.AllowDecimalPoint;
                    CultureInfo culture = CultureInfo.CreateSpecificCulture("en-GB");
                    if (double.TryParse(answer.Replace(',','.'), style, culture, out value))
                        return getTemparature(value);
                    return -1;
                    //throw new ArgumentException("wrong input by user");                  
            }
            return value;
        }
        /// <summary>
        /// linear function for temperature
        /// points: 36.6 -> 0, 40.6 -> 1
        /// </summary>
        /// <param name="temp"></param>
        /// <returns></returns>
        private double getTemparature(double temp)
        {
            double value = 0;
            if (temp <= 36.6)
                return 0;
            else if (temp >= 40.6)
                return 1;

            double a = 0.25;
            double b = -9.15;
            return a * temp + b;
        }


        private void nextQuestion()
        {
            while (counter < tree.symList.Count && (tree.symList[counter].value >= 0 || !tree.symList[counter].QuestionToAsk))
            {
                counter++;
            }
            if (counter >= tree.symList.Count)
            {
                questionLabel.Content = "Zakończono!";
                answerBox.Visibility = Visibility.Hidden;
                nextBtn.IsEnabled = false;
                showButton.Visibility = Visibility.Visible;
                //resultDataGrid.Visibility = Visibility.Visible;
                numberLabel.Visibility = Visibility.Visible;
                numberBox.Visibility = Visibility.Visible;
                MainGrid.ColumnDefinitions[0].Width = new GridLength(0);
                MainGrid.ColumnDefinitions[1].Width = GridLength.Auto;
                numberBox.Focus();
                tree.CalculateResults(tree.symList[0].value, 0); //tutaj po zakończeniu pytań obliczane są prawdopodobieństwa
                tree.Sort();
            }
            else
            {
                string content = question + tree.symList[counter].symptom + "?";
                switch (tree.symList[counter].type)
                {
                    case SymptomsType.Value:
                        content += " (0-10)";
                        break;
                    case SymptomsType.YesNo:
                        content += " (tak/nie)";
                        break;
                    case SymptomsType.Linear:
                        content += " (wartość)";
                        break;
                }
                questionLabel.Content = content;
                answerBox.Text = "";
            }
        }

        private void ShowButton_OnClick_Click(object sender, RoutedEventArgs e)
        {
            int n;
            if (!Int32.TryParse(numberBox.Text, out n)||n<0)
                n = 1;
            if (n > tree.disList.Count)
                n = tree.disList.Count;
            StringBuilder sb = new StringBuilder();
            sb.Append("Choroba     ").Append("Prawdopodobieństwo\n");
            for (int i = 0; i < n; i++)
            {
                sb.Append(tree.disList[i].disease).Append("    ").Append(tree.disList[i].probability).Append("\n");
            }
            resultTextBlock.Text = sb.ToString();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter && nextBtn.IsEnabled == true)
                Button_Click(sender, e);
            else if (e.Key == Key.Enter && showButton.Visibility == Visibility.Visible)
                ShowButton_OnClick_Click(sender, e);
        }
        
    }
    
}
